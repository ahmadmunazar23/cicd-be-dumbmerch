# Stage 1: Build the Go application
FROM golang:1.16-alpine AS build

WORKDIR /app

# Copy all files, including subdirectories
COPY . .

# Build the Go application
RUN go build -o main .

# Stage 2: Create a minimal image to run the application
FROM alpine:latest

WORKDIR /app

# Copy all files from the build stage
COPY --from=build /app .

# Expose the port the application runs on
EXPOSE 5000

# Command to run the application
CMD ["./main"]

